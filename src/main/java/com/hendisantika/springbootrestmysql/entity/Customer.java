package com.hendisantika.springbootrestmysql.entity;

import lombok.*;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/09/18
 * Time: 04.38
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "customer")
@Data
@Getter
@Setter
//@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private int age;

    @Column(name = "active")
    private boolean active;

    public Customer(String name, int age) {
        this.name = name;
        this.age = age;
        this.active = false;
    }

}
